<?php

namespace App\Controllers;

use App\Class\Signup;
class SignupController
{
    private string $uid;
    private string $pwd;
    private string $pwdRepeat;
    private string $email;
    private Signup $signup;

    public function __construct(string $uid, string $pwd, string $pwdRepeat, string $email)
    {
        $this->uid = $uid;
        $this->pwd = $pwd;
        $this->pwdRepeat = $pwdRepeat;
        $this->email = $email;
        $this->signup = new Signup();
    }

    public function signupUser(): void
    {
        if(!$this->emptyInput()) {
            header("location:../Inc/signupError.php?error=emptyinput");
            exit();
        }
        if(!$this->invalidUid()) {
            header("location:../Inc/signupError.php?error=username");
            exit();
        }
        if(!$this->invalidEmail()) {
            header("location:../Inc/signupError.php?error=email");
            exit();
        }
        if(!$this->pwdMatch()) {
            header("location:../Inc/signupError.php?error=passwordmatche");
            exit();
        }
        if(!$this->uidTakenCheck()) {
            header("location:../Inc/signupError.php?error=useroremailtaken");
            exit();
        }
        $this->signup->setUser($this->uid, $this->pwd, $this->email);
    }

    private function emptyInput()
    {
        $result = true;
        if (empty($this->uid) || empty($this->pwd) || empty($this->pwdRepeat) || empty($this->email)) {
            $result = false;
        }
        return $result;
    }

    private function invalidUid(): bool
    {
        $result = true;
        if (!preg_match("/^[a-zA-Z0-9]*$/", $this->uid)) {
            $result = false;
        }
        return $result;
    }

    private function invalidEmail(): bool
    {
        $result = true;
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $result = false;
        }
        return $result;
    }

    private function pwdMatch(): bool
    {
        $result = true;
        if ($this->pwd !== $this->pwdRepeat) {
            $result = false;
        }
        return $result;
    }

    private function uidTakenCheck(): bool
    {
        $result = true;
        if(!$this->signup->checkUser($this->uid, $this->email)) {
            $result = false;
        }
        return $result;
    }
}