<?php

namespace App\Controllers;

use App\Class\Login;
class LoginController
{
    private string $uid;
    private string $pwd;
    private string $email;
    private Login $login;

    public function __construct($uid, $pwd)
    {
        $this->uid = $uid;
        $this->pwd = $pwd;
        $this->login = new Login();
    }

    public function loginUser(): void
    {
        if(!$this->emptyInput()) {
            header("location:../Inc/loginError.php?error=emptyinput");
            exit();
        }
        $this->login->getUser($this->uid, $this->pwd);
    }

    private function emptyInput(): bool
    {
        $result = true;
        if (empty($this->uid) || empty($this->pwd)) {
            $result = false;
        }
        return $result;
    }
}