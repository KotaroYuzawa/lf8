<?php

namespace App\Class;
use App\SRC\Database;
use PDO;

class Login
{
    public Database $db;

    public function __construct(Database $db = null)
    {
        $this->db = $db ?? Database::getInstance();
    }

    public function getUser($uid, $pwd): void
    {
        $sql = 'SELECT user_pwd FROM users WHERE user_uid = :user_uid;';
        $stm = $this->db->getConnection()->prepare($sql);
        $stm->bindParam(':user_uid', $uid);

        if (!$stm->execute()) {
            $stm = null;
            header("location:../Inc/loginError.php?stmtfailed");
            exit();
        }

        $pwdHashed = $stm->fetchAll(PDO::FETCH_ASSOC);
        if (count($pwdHashed) == 0) {
           $stm = null;
            header("location:../Inc/loginError.php?error=usernotfound");
        }

        $checkPwd = password_verify($pwd, $pwdHashed[0]['user_pwd']);

        if (!$checkPwd){
            $stm = null;
            header("location:../Inc/loginError.php?error=wrongpassword");
            exit();
        } elseif ($checkPwd == true) {
            $sql = 'SELECT * FROM users WHERE user_uid = ? OR user_email = ? AND user_pwd = ?;';
            $stm = $this->db->getConnection()->prepare($sql);

            if (!$stm->execute(array($uid, $uid, $pwd))) {
                $stm = null;
                header("location:../Inc/loginError.php?error=stmtfailed");
                exit();
            }

            $user = $stm->fetchAll(PDO::FETCH_ASSOC);
            if (count($user) == 0) {
                $stm = null;
                header("location:../Inc/loginError.php?error=usernotfound");
            }

            session_start();
            $_SESSION['userid'] = $user[0]['user_id'];
            $_SESSION['useruid'] = $user[0]['user_uid'];
            $_SESSION['useremail'] = $user[0]['user_email'];
         }
        $stm = null;
    }
}
