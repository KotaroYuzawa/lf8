<?php

namespace App\Class;
use App\SRC\Database;
use PDO;

class Signup
{
    public Database $db;

    public function __construct(Database $db = null)
    {
        $this->db = $db ?? Database::getInstance();
    }

    public function setUser($uid, $pwd, $email): void
    {
        $sql = 'INSERT INTO users (user_uid, user_pwd, user_email) VALUES(:user_uid, :user_pwd, :user_email);';
        $stm = $this->db->getConnection()->prepare($sql);
        $stm->bindParam(':user_uid', $uid);

        $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);
        $stm->bindParam(':user_pwd', $hashedPwd);
        $stm->bindParam(':user_email', $email);
        if (!$stm->execute()) {
            $stm = null;
            header("location:../Inc/signupError.php?error=stmtfailed");
            exit();
        }
        $stm = null;
    }

    public function checkUser($uid, $email)
    {
        $sql = 'SELECT user_uid FROM users WHERE user_uid = ? OR user_email = ?;';
        $stm = $this->db->getConnection()->prepare($sql);

        if (!$stm->execute(array($uid, $email))) {
            $stm = null;
            header("location:../Inc/signupError.php?error=stmtfailed");
            exit();
        }
        $resultCheck = true;
        $userData = $stm->fetchAll(PDO::FETCH_ASSOC);

        if (!count($userData) == 0) {
            $resultCheck = false;
        }

        return $resultCheck;
    }
}