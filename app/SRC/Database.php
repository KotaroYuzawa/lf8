<?php

namespace App\SRC;

use PDO;

class Database
{
    private static $instance;
    private $connection;

    private function __construct()
    {
        if(empty(self::$instance)) {
            $host = 'localhost';
            $user = 'root';
            $pass = 'pass';
            $dbname = 'Booking';
            //$port = '3306';

            try{
                $this->connection = new PDO("mysql:host=$host;dbname=$dbname",$user,$pass);
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(\Exception $e) {
                echo "Connection failed: " . $e -> getMessage();
            }
        }
    }
    public static function getInstance ()
    {
        if (self::$instance == null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->connection;
    }
}
