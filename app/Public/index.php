<?php
  session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Home</title>
  <link rel="stylesheet" href="../SRC/style.css">
</head>
<body>
<nav>
  <ul>
    <li><a href="#">Home</a></li>
    <?php
      if (isset($_SESSION['userid']))
      {
    ?>
    <li><a href="../Public/account.php"><?php echo $_SESSION['useruid']; ?></a></li>
    <li><a href="../Inc/logout.inc.php">LOGOUT</a></li>
    <?php
      } else {
    ?>
    <li><a href="signup.php">Sign up</a></li>
    <li><a href="login.php">Log in</a></li>
    <?php
    }
    ?>
  </ul>
</nav>
<div class="form-center">
  <h4>Home</h4>
</div>
</body>