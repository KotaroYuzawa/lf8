<?php
session_start()
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="../SRC/style.css">
</head>
<body>
<nav>
    <ul>
        <li><a href="index.php">Home</a></li>
        <?php
        if (isset($_SESSION['userid']))
        {
            ?>
            <li><a href="../Public/account.php"><?php echo $_SESSION['useruid']; ?></a></li>
            <li><a href="../Inc/logout.inc.php">LOGOUT</a></li>
            <?php
        } else {
            ?>
            <li><a href="signup.php">Sign up</a></li>
            <li><a href="login.php">Log in</a></li>
            <?php
        }
        ?>
    </ul>
</nav>
<div class="signup">
    <h4>Sign up</h4>
    <form action="../Inc/signup.inc.php" method="post">
        <input type="text" name="uid" placeholder="Username">
        <input type="password" name="pwd" placeholder="Password">
        <input type="password" name="pwdRepeat" placeholder="Repeat Password">
        <input type="text" name="email" placeholder="E-mail">
        <br>
        <button type="submit" name="submit">Sign Up</button>
    </form>
    <?php
    if (isset($_SESSION['errorMsg']))
    {
        ?>
        <span><?php echo $_SESSION['errorMsg']; ?></span>
        <?php
    }
    ?>
</div>
</body>

<?php
session_unset();
session_destroy();
?>