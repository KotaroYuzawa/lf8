<?php

session_start();

$errorMsg = $_GET['error'];

if ($errorMsg === 'emptyinput') {
    $_SESSION['errorMsg'] = 'Please fill out all input fields';
} else if ($errorMsg === 'username') {
    $_SESSION['errorMsg'] = 'You can use only letters or numbers for username';
}else if ($errorMsg === 'email') {
    $_SESSION['errorMsg'] = 'Invalid email';
}else if ($errorMsg === 'passwordmatche') {
    $_SESSION['errorMsg'] = 'Password does not match';
}else if ($errorMsg === 'useroremailtaken') {
    $_SESSION['errorMsg'] = 'Username or email is already used by other users';
}

header('location:../Public/signup.php');