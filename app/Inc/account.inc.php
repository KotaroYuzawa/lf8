<?php

session_start();

use App\Controllers\AccountController;
use App\Class\User;

$userId = $_SESSION['userid'];

$user = new AccountController($userId);

header('location:../Public/account.php');