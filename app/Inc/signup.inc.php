<?php

use App\Classes\Signup;
use App\Controllers\SignupController;

require_once '../../vendor/autoload.php';
include '../Controllers/SignupController.php';

if (isset($_POST['submit'])) {
    $uid = $_POST['uid'];
    $pwd = $_POST['pwd'];
    $pwdRepeat = $_POST['pwdRepeat'];
    $email = $_POST['email'];

    $signup = new SignupController($uid, $pwd, $pwdRepeat, $email);
    $signup->signupUser();
    header("location:../Public/index.php");
}