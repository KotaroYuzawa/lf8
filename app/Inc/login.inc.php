<?php

use App\Classes\Signup;
use App\Controllers\LoginController;

require_once '../../vendor/autoload.php';

if (isset($_POST['submit'])) {
    $uid = $_POST['uid'];
    $pwd = $_POST['pwd'];

    $login = new LoginController($uid, $pwd);
    $login->loginUser();
    header("location:../Public/index.php");
}