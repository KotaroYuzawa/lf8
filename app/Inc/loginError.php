<?php

session_start();

$errorMsg = $_GET['error'];

if ($errorMsg === 'emptyinput') {
    $_SESSION['errorMsg'] = 'Please fill out all input fields';
} else if ($errorMsg === 'wrongpassword') {
    $_SESSION['errorMsg'] = 'Wrong password';
} else if ($errorMsg === 'stmtfailed') {
    $_SESSION['errorMsg'] = 'Somethig wrong with database connection';
} else if ($errorMsg === 'usernotfound') {
    $_SESSION['errorMsg'] = 'User not found';
}

header('location:../Public/login.php');