<?php

use PHPUnit\Framework\TestCase;
use App\Controllers\SignupController;

class SignupControllerTest extends TestCase
{
    public function testSignupControllerInstantiation()
    {
        $this->signupController = new SignupController('testuser', 'password123', 'password123', 'testuser@example.com');
        $this->assertInstanceOf(SignupController::class, $this->signupController);
    }

    public function testSignupData()
    {
        $signupController = new SignupController('testuser', 'password123', 'password123', 'testuser@example.com');
        $reflector = new ReflectionClass($signupController);

        $uidProperty = $reflector->getProperty('uid');
        $uidProperty->setAccessible(true);
        $uidValue = $uidProperty->getValue($signupController);
        $this->assertSame('testuser', $uidValue);
        $this->assertIsString($uidValue);

        $pwdProperty = $reflector->getProperty('pwd');
        $pwdProperty->setAccessible(true);
        $pwdValue = $pwdProperty->getValue($signupController);
        $this->assertSame('password123', $pwdValue);
        $this->assertIsString($pwdValue);

        $pwdRepeatProperty = $reflector->getProperty('pwdRepeat');
        $pwdRepeatProperty->setAccessible(true);
        $pwdRepeatValue = $pwdRepeatProperty->getValue($signupController);
        $this->assertSame('password123', $pwdRepeatValue);
        $this->assertIsString($pwdRepeatValue);

        $emailProperty = $reflector->getProperty('email');
        $emailProperty->setAccessible(true);
        $emailValue = $emailProperty->getValue($signupController);
        $this->assertSame('testuser@example.com', $emailValue);
        $this->assertIsString($emailValue);
    }
}
