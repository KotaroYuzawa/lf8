<?php

use App\Class\Signup;
use App\SRC\Database;
use PHPUnit\Framework\TestCase;

class SignupTest extends TestCase
{
    private $signup;
    private $pdoStatementMock;

    protected function setUp(): void
    {
        $dbMock = $this->createMock(Database::class);
        $pdoMock = $this->createMock(PDO::class);
        $this->pdoStatementMock = $this->createMock(PDOStatement::class);

        $dbMock->method('getConnection')->willReturn($pdoMock);
        $pdoMock->method('prepare')->willReturn($this->pdoStatementMock);

        $this->signup = new Signup($dbMock);
    }

    public function testSetUser()
    {
        $this->pdoStatementMock->expects($this->once())->method('execute')->willReturn(true);
        $this->signup->setUser('testuser', 'password123', 'test@example.com');
        $this->assertTrue(true);
    }

    public function testCheckUser()
    {
        $this->pdoStatementMock->method('fetchAll')->willReturn([]);
        $this->pdoStatementMock->expects($this->once())->method('execute')->willReturn(true);
        $result = $this->signup->checkUser('testuser', 'test@example.com');
        $this->assertTrue($result);
    }
}
