<?php

use PHPUnit\Framework\TestCase;
use App\Controllers\LoginController;

class LoginControllerTest extends TestCase
{
    public function testLoginControllerInstantiation()
    {
        $loginController = new LoginController('testuser', 'password123');
        $this->assertInstanceOf(LoginController::class, $loginController);
    }

    public function testLoginData()
    {
        $loginController = new LoginController('testuser', 'password123');
        $reflector = new ReflectionClass($loginController);

        $uidProperty = $reflector->getProperty('uid');
        $uidProperty->setAccessible(true);
        $uidValue = $uidProperty->getValue($loginController);
        $this->assertSame('testuser', $uidValue);
        $this->assertIsString($uidValue);

        $pwdProperty = $reflector->getProperty('pwd');
        $pwdProperty->setAccessible(true);
        $pwdValue = $pwdProperty->getValue($loginController);
        $this->assertSame('password123', $pwdValue);
        $this->assertIsString($pwdValue);
    }
}
